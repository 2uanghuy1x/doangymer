﻿using System;

namespace Cinema.DTO.DtoGym
{
    public class UpdateGymDto
    {
        public Guid LastModifierUserId { get; set; }
        public Guid Id { get; set; }
        public string EpisodePackName { get; set; }
        public int EpisodePackMonth { get; set; }
        public int Price { get; set; }
        public int Type { get; set; }
    }
}
