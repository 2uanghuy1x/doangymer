﻿namespace Cinema.DTO.DtoGym
{
    public class SearchGymDto
    {
        public string EpisodePackName { get; set; }
        public int EpisodePackMonth { get; set; }
        public int Price { get; set; }
        public int Type { get; set; }
    }
}
