﻿using System.Data;
using System;

namespace Cinema.DTO.DtoGym
{
    public class GetAllGymDto
    {
        public GetAllGymDto(DataRow row)
        {
            //this.CinemaId = (Guid)row["CinemaId"];
            this.Id = (Guid)row["Id"];
            this.EpisodePackName = row["EpisodePackName"].ToString();
            this.EpisodePackMonth = (int)row["EpisodePackMonth"];
            //this.Size = (int)row["Size"];
            this.Type = (int)row["Type"];
            this.Price = (int)row["Price"];
        }
        public Guid Id { get; set; }
        // public Guid CinemaId { get; set; }
        public string EpisodePackName { get; set; }
        public int EpisodePackMonth { get; set; }
        public int Price { get; set; }
        public int Type { get; set; }
    }
}
