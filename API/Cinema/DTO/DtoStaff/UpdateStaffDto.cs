﻿using System;

namespace Cinema.DTO.DtoStaff
{
    public class UpdateStaffDto
    {
        public Guid LastModifierUserId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime DoB { get; set; }
        public byte Sex { get; set; }
        public int Type { get; set; }
    }
}
