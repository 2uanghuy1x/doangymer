﻿using System.Data;
using System;

namespace Cinema.DTO.DtoStaff
{
    public class GetAllStaffDto
    {
        public GetAllStaffDto(DataRow row)
        {
            this.Id = (Guid)row["Id"];
            this.Name = row["Name"].ToString();
            this.Address = row["Address"].ToString();
            this.DoB = (DateTime)row["DoB"];
            this.Type = (int)row["Type"];
            this.Sex = (byte)row["Sex"];
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime DoB { get; set; }
        public byte Sex { get; set; }
        public int Type { get; set; }
    }
}
