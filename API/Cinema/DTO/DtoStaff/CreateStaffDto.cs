﻿using System;

namespace Cinema.DTO.DtoStaff
{
    public class CreateStaffDto
    {
        public Guid CreatorUserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime DoB { get; set; }
        public byte Sex { get; set; }
        public int Type { get; set; }
    }
}
