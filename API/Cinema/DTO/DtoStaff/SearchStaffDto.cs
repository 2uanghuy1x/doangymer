﻿using System;

namespace Cinema.DTO.DtoStaff
{
    public class SearchStaffDto
    {
        public string Name { get; set; }
        public byte Sex { get; set; }
        public int Type { get; set; }
    }
}
