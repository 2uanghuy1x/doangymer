﻿using Cinema.DTO.DtoGym;
using Cinema.DTO;
using Cinema.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using Cinema.DTO.DtoStaff;

namespace Cinema.Controllers
{
    [ApiController]
    [Route("api/staff")]
    public class StaffController : DBConnect
    {
        [HttpPost("search")]
        public List<GetAllStaffDto> Search(SearchStaffDto input)
        {
            conn.Open();
            //string id = string.Format("00000000-0000-0000-0000-000000000000");
            //if (input.cinemaId == null) input.cinemaId = new Guid(id);
            string sql = string.Format("exec SearchGym  @Name = '" + input.Name + "', @Sex = '" + input.Sex + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            DataTable data = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(data);
            var list = new List<GetAllStaffDto>();
            foreach (DataRow i in data.Rows)
            {
                GetAllStaffDto dto = new GetAllStaffDto(i);
                list.Add(dto);
            }
            conn.Close();
            return list.ToList();
        }

        [HttpPost("create")]
        public bool Create(CreateStaffDto input)
        {
            conn.Open();
            string sql = string.Format("exec CreateGym @CreatorUserId = '" + input.CreatorUserId + "', @Name = N'" + input.Name + "', @Sex = '" + input.Sex + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
        [HttpPut("update")]
        public bool Update(UpdateStaffDto input)
        {
            conn.Open();
            string sql = string.Format("exec UpdateGym @LastModifierUserId = '" + input.LastModifierUserId + "', @Id = '" + input.Id + "', @Name = N'" + input.Name + "', @Sex = '" + input.Sex + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
        [HttpDelete("delete")]
        public bool Delete(DeleteDto input)
        {
            conn.Open();
            string sql = string.Format("update UpdateGym set IsDeleted = 1, DeleteTime = getdate(), DeleterUserId = '" + input.DeleterUserId + "' where Id = '" + input.Id + "'");
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
    }
}
