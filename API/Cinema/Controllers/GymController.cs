﻿using Cinema.DTO.DtoFood;
using Cinema.DTO;
using Cinema.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Cinema.DTO.DtoGym;
using System.Linq;

namespace Cinema.Controllers
{
    [ApiController]
    [Route("api/gym")]
    public class GymController : DBConnect
    {
        [HttpPost("search")]
        public List<GetAllGymDto> Search(SearchGymDto input)
        {
            conn.Open();
            //string id = string.Format("00000000-0000-0000-0000-000000000000");
            //if (input.cinemaId == null) input.cinemaId = new Guid(id);
            string sql = string.Format("exec SearchGym  @Name = '" + input.EpisodePackName + "', @Month = '" + input.EpisodePackMonth + "', @Price = '" + input.Price + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            DataTable data = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
            adapter.Fill(data);
            var list = new List<GetAllGymDto>();
            foreach (DataRow i in data.Rows)
            {
                GetAllGymDto dto = new GetAllGymDto(i);
                list.Add(dto);
            }
            conn.Close();
            return list.ToList();
        }
       
        [HttpPost("create")]
        public bool Create(CreateGymDto input)
        {
            conn.Open();
            string sql = string.Format("exec CreateGym @CreatorUserId = '" + input.CreatorUserId + "', @Name = N'" + input.EpisodePackName + "', @Month = '" + input.EpisodePackMonth + "', @Price = " + input.Price + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
        [HttpPut("update")]
        public bool Update(UpdateGymDto input)
        {
            conn.Open();
            string sql = string.Format("exec UpdateGym @LastModifierUserId = '" + input.LastModifierUserId + "', @Id = '" + input.Id + "', @Name = N'" + input.EpisodePackMonth + "', @Month = '" + input.EpisodePackMonth + "', @Price = '" + input.Price + "',@Type =" + input.Type);
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
        [HttpDelete("delete")]
        public bool Delete(DeleteDto input)
        {
            conn.Open();
            string sql = string.Format("update UpdateGym set IsDeleted = 1, DeleteTime = getdate(), DeleterUserId = '" + input.DeleterUserId + "' where Id = '" + input.Id + "'");
            SqlCommand sqlCommand = new SqlCommand(sql, conn);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            conn.Close();
            return false;
        }
    }
}
