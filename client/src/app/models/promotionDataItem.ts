export interface PromotionDataItem {
    id: string;
    code: string;
    price: number;
    startDate: Date;
    endDate: Date;
}
export class searchPromotion {
    id!: string;
    code: string;
    price: number;
    startDate: Date;
    endDate: Date;
}
