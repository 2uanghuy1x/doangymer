import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GymService {
  baseUrl = environment.baseUrl + 'gym';

  constructor(private http: HttpClient) { }

  searchGymDataItem(payload?: any): Observable<any> {
    return this.http.post(this.baseUrl + '/search?', payload);
  }
  createGym(payload: any): Observable<any> {
    return this.http.post(this.baseUrl + '/create', payload);
  }
  updateGym(payload: any): Observable<any> {
    return this.http.put(this.baseUrl + '/update', payload);
  }
  deleteGym(payload: any): Observable<any> {
    return this.http.delete(this.baseUrl + '/delete', payload);
  }
}
