import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StaffService {
  baseUrl = environment.baseUrl + 'staff';

  constructor(private http: HttpClient) { }
  searchStaffDataItem(payload?: any): Observable<any> {
    return this.http.post(this.baseUrl + '/search?', payload);
  }
  createStaff(payload: any): Observable<any> {
    return this.http.post(this.baseUrl + '/create', payload);
  }
  updateStaff(payload: any): Observable<any> {
    return this.http.put(this.baseUrl + '/update', payload);
  }
  deleteStaff(payload: any): Observable<any> {
    return this.http.delete(this.baseUrl + '/delete', payload);
  }
}
