export enum Size
{
    S = 1,
    M = 2,
    L = 3,
}
export enum Type
{
    BotDinhDuong = 1,
    PhuKien = 2,
}
export enum TypeStaff
{
    BanHang = 1,
    HuanLuyenVien = 2,
}

export enum TypeGym
{
    Gym = 1,
    YoGa = 2,
}
